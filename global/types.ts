/** 分页查询参数 */
export type PagingParams = {
  /** 页码，默认0 */
  page?: number
  /** 单页参训数量，默认10 */
  size?: number
}

/** 分页响应数据 */
export type PagingResponse = {
  page: number
  total: number
}

// —————————————————————————————————User—————————————————————————————————————
/** 注册表单 */
export type RegisterForm = {
  /** 用户电话号码 */
  phone: string
  /** 用户昵称 */
  name: string
  /** 用户密码 */
  psd: string
  /** 手机短信验证码 */
  code: string
}

/** 密码登录表单 */
export type LoginPsdForm = {
  phone: string
  psd: string
}
/** 短信登录表单 */
export type LoginSmsForm = {
  phone: string
  code: string
}
/** 登录响应 */
export type LoginResponse = {
  id: number
  name: string
  phone: string
  token: string
  locale: 'zhCN' | 'enUS'
}

/** 短信类型 */
export enum SmsType {
  /** 注册 */
  register,
  /** 登录 */
  login
}

// —————————————————————————————————Nas—————————————————————————————————————
/** 文件类型 */
export enum FileType {
  /** 所有 */
  all,
  /** 其他 */
  other,
  /** 图片 */
  image,
  /** 视频 */
  video,
  /** 音频 */
  audio,
  /** 文档 */
  document,
  /** 安装包 */
  package,
  /** 压缩包 */
  archive,
  /** 文件夹 */
  dir
}

export const FileTypeMap = ['all', 'others', 'imgs', 'videos', 'audios', 'docs', 'packages', 'archives', 'dir']

/** Nas 文件信息 */
export type NasFile = {
  /** 文件/文件夹id */
  id: number
  /** 用户id */
  uid: number
  /** 文件名 */
  fileName: string
  /** 所在文件夹名 */
  parentPath: string
  /** 文件路径 */
  filePath: string
  /** 文件大小，单位字节 */
  fileSize?: number
  /** 文件类型 */
  fileType: Omit<FileType, FileType.all | FileType.dir>
  /** 已上传大小，单位字节 */
  uploaded?: number
  /** 0：未上传完毕， 1：已上传完毕 */
  uploadFinish: 1 | 0
  /** 缩略图后缀 */
  thumbnailExt?: string
  /** 创建时间 */
  createTime: string
  /** 拥有子文件 */
  hasChild?: boolean
}

// —————————————————————————————————Nas文件列表—————————————————————————————————————
/** 查询参数 */
export type GetNasFileListParams = PagingParams & {
  /** 查询的文件类型 */
  fileType?: FileType
  /** 查询所在文件夹下的所有文件 */
  parentPath?: string
  isUploaded?: boolean
}
/** 查询响应 */
export type GetNasFileListResponse = PagingResponse & {
  list: NasFile[]
}

/** 上传文件表单 */
export type UploadNasFileForm = {
  id?: string
  parentPath: string
  fileName: string
  fileSize: string
  fileType: FileType
}
