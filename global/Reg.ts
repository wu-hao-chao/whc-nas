class Reg {
  static email = /^[A-Za-z0-9\u4e00-\u9fa5]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/
  static isEmail = (v: string): boolean => /^[A-Za-z0-9\u4e00-\u9fa5]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/.test(v)
  static phone = /^(?:(?:\+|00)86)?1\d{10}$/
  static isPhone = (v: string): boolean => /^(?:(?:\+|00)86)?1\d{10}$/.test(v)
  static chinese = /^(?:[\u4e00-\u9fa5·]{2,16})$/
  static isChinese = (v: string): boolean => /^(?:[\u4e00-\u9fa5·]{2,16})$/.test(v)
  static num = /^[0-9]+$/
  static isNum = (v: string): boolean => /^[0-9]+$/.test(v)
  static code = /^\d{6}$/
  static is6Num = (v: string): boolean => /^\d{6}$/.test(v)
  static uppercase = /^[A-Z]+$/
  static isUppercase = (v: string): boolean => /^[A-Z]+$/.test(v)
  static Lowercase = /^[a-z]+$/
  static isLowercase = (v: string): boolean => /^[a-z]+$/.test(v)
  static isLetter = (v: string): boolean => /^[A-Za-z]+$/.test(v)
  static imgUrl = /^https?:\/\/(.+\/)+.+(\.(gif|png|jpg|jpeg|webp|svg|psd|bmp|tif))$/i
  static isImgUrl = (v: string): boolean => /^https?:\/\/(.+\/)+.+(\.(gif|png|jpg|jpeg|webp|svg|psd|bmp|tif))$/i.test(v)
  static inLength = (v: string, min: number, max?: number): boolean => {
    if (!v) return false
    if (v.length < min) return false
    if (max && v.length > max) return false
    return true
  }
  static nasParentPath = /^([^@<>"\s\\\/\:\*\?]+@@@)*$/
  static isNasParentPath = (v: string): boolean => /^([^<>"\s\\\/\:\*\?]+@@@)*$/.test(v)
  static nasFilePath = /^([^@<>"\s\\\/\:\*\?]+@@@)+.+$/
  static isNasFilePath = (v: string): boolean => /^([^<>"\s\\\/\:\*\?]+@@@)+.+$/.test(v)
}

export default Reg
