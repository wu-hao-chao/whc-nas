# Nodejs 使用 TypeScript

1. 初始化nodejs项目：

   ```bash
   md ts-node-project
   cd ts-node-project
   npm init --yes
   ```

2. 下载 TypeScript 相关依赖：

   ```bash
   npm i nodemon ts-node typescript @types/node -D
   ```

3. 初始化 `tsconfig.json` 文件：

   ```bash
   tsc --init
   ```

4. 添加 `package.json` 中的 `script ` 脚本命令：

   ```json
   {
   	"scripts": {
   		"dev": "nodemon --watch src -e ts,tsx --exec ts-node src/index.ts",
   		"start": "ts-node src/ index.ts"
   	}
   }
   ```

5. 添加入口文件 `src/index.ts` ：

   ```typescript
   const msg: string = 'Hello! Nodejs and TypeScript!'
   console.log(msg)
   ```

6. 开发模式运行项目（文件改动时重启服务）：

   ```bash
   npm run dev
   ```

7. 生产环境运行项目：

   ```bash
   npm run start
   ```

   

