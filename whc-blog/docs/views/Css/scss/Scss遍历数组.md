# Scss 遍历数组

scss 源文件：

```scss
$colorList: (
	danger: #f95556,
	warning: #e6a23c,
	success: #67c23a,
	info: #409eff,
	auxiliary: #999,
	black: #333,
	white: #fff
);

@each $key, $item in $colorList {
	._#{ $key } {
		color: $item;
	}
	._#{ $key }-bg {
		background-color: $item;
	}
	._#{ $key }-btn {
		background-color: $item;
		transition: 0.1s;
		&:active {
			filter: brightness(90%);
		}
	}
}
```

生成的 css 文件：

```css
._danger {
	color: #f95556;
}

._danger-bg {
	background-color: #f95556;
}

._danger-btn {
	background-color: #f95556;
	transition: 0.1s;
}
._danger-btn:active {
	filter: brightness(90%);
}

._warning {
	color: #e6a23c;
}

._warning-bg {
	background-color: #e6a23c;
}

._warning-btn {
	background-color: #e6a23c;
	transition: 0.1s;
}
._warning-btn:active {
	filter: brightness(90%);
}

._success {
	color: #67c23a;
}

._success-bg {
	background-color: #67c23a;
}

._success-btn {
	background-color: #67c23a;
	transition: 0.1s;
}
._success-btn:active {
	filter: brightness(90%);
}

._info {
	color: #409eff;
}

._info-bg {
	background-color: #409eff;
}

._info-btn {
	background-color: #409eff;
	transition: 0.1s;
}
._info-btn:active {
	filter: brightness(90%);
}

._auxiliary {
	color: #999;
}

._auxiliary-bg {
	background-color: #999;
}

._auxiliary-btn {
	background-color: #999;
	transition: 0.1s;
}
._auxiliary-btn:active {
	filter: brightness(90%);
}

._black {
	color: #333;
}

._black-bg {
	background-color: #333;
}

._black-btn {
	background-color: #333;
	transition: 0.1s;
}
._black-btn:active {
	filter: brightness(90%);
}

._white {
	color: #fff;
}

._white-bg {
	background-color: #fff;
}

._white-btn {
	background-color: #fff;
	transition: 0.1s;
}
._white-btn:active {
	filter: brightness(90%);
}
```
