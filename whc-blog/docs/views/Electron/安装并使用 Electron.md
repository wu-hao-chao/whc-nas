---
head:
  - - meta
    - name: description
      content: Electron安装使用
  - - meta
    - name: keywords
      content: Electron 使用
---

# 安装并使用 Electron

1. 在命令行初始化项目并安装 electron 模块

   ```
   md test
   cd test
   npm init --yes
   npm i electron
   ```

2. 打开 package.json，修改 "main"，和 "scripts" 项：

   ```
   {
     "name": "test",
     "version": "1.0.0",
     "description": "",
     "main": "main.js",
     "scripts": {
       "test": "echo \"Error: no test specified\" && exit 1",
       "electron": "electron ."
     },
     "keywords": [],
     "author": "",
     "license": "ISC"
   }
   ```

3. 创建 main.js 文件并写入以下代码：

   ```js
   const { app, BrowserWindow } = require('electron')

   app.on('ready', () => {
   	const mainWindow = new BrowserWindow({
   		width: 500,
   		height: 500
   	})
   })
   ```

4. 在终端输入命令启动项目

   ```
   npm run electron
   ```

5. 创建一个 index.html 文件，并随意写入内容，例如：

   ```html
   <!DOCTYPE html>
   <html lang="en">
   	<head>
   		<meta charset="UTF-8" />
   		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
   		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
   		<title>Document</title>
   	</head>
   	<body>
   		<h1>Electon的安装和使用</h1>
   	</body>
   </html>
   ```

6. 重新打开 main.js，添加以下代码：

   ```js
   const { app, BrowserWindow } = require('electron')

   app.on('ready', () => {
   	const mainWindow = new BrowserWindow({
   		width: 500,
   		height: 500
   	})

   	mainWindow.loadFile('./src/index.html')
   })
   ```

7. 重新运行项目

   ```
   npm run electron
   ```

## 配置热加载

1. 下载 electron-reloader 模块

   ```
   npm i electron-reloader -D
   ```

2. 在 main.js 添加以下代码

   ```js
   const { app, BrowserWindow } = require('electron')
   const reloader = require('electron-reloader')

   reloader(module)

   app.on('ready', () => {
   	const mainWindow = new BrowserWindow({
   		width: 500,
   		height: 500
   	})

   	mainWindow.loadFile('./src/index.html')
   })
   ```

3. 启动项目

   ```
   npm run electron
   ```
