# react 的虚拟dom的实现原理

React 是把真实的 DOM 树转换为 JS 对象树，也就是 Virtual DOM。每次数据更新后，重新计算 VM，并和上一次生成的 VM 树进行对比，对发生变化的部分进行批量更新。除了性能之外，VM 的实现最大的好处在于和其他平台的集成。

比如我们一个真是的 DOM 是这样的

```js
<button class="myButton">
  <span>this is button</span>
</button>
```

那么在转化为 VM 之后就是这样的

```js
{
  type: 'button',
  props: {
  	className: 'myButton',
    children: [{
      type: 'span',
      props: {
        type: 'text'
        children: 'this is button'
      }
    }]
  }
}
```