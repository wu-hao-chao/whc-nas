# setState 是同步，还是异步的？

## react18之前。

setState在不同情况下可以表现为异步或同步。

在Promise的状态更新、js原生事件、setTimeout、setInterval..中是同步的。

在react的合成事件中，是异步的。

------

## react18之后。

setState都会表现为异步（即批处理）。
[官方详细说明。](https://github.com/reactwg/react-18/discussions/21)

------

## react17和react18中setState在setTimeout中的表现

实例代码：

```js
export default class Test extends Component {
  state = {
    num: 0
  }
  hClick = () => {
    setTimeout(() => {
      console.log('before----', this.state.num)
      this.setState({num: this.state.num+1})
      console.log('after----', this.state.num)
    }, 1000)
  }
  render() {
    return (
    <div>
      React 17/18
      <hr />
      {this.state.num}
      <button onClick={this.hClick}>+1</button>
    </div>)
  }
}
```

同样代码在react17和react18中的表现如下：



![img](https://p9-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/7fdf60adef9d43e6bfa574d616693707~tplv-k3u1fbpfcp-zoom-in-crop-mark:3024:0:0:0.awebp?)

预览

![img](https://p1-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/88f168d045374f16b1ebb2cf0955d94d~tplv-k3u1fbpfcp-zoom-in-crop-mark:3024:0:0:0.awebp?)

预览

------

## react18之前版本的解释

在React中，如果是由React引发的事件处理（比如通过onClick引发的事件处理），调用setState不会同步更新this.state，除此之外的setState调用会同步执行this.state 。所谓“除此之外”，指的是绕过React通过addEventListener直接添加的事件处理函数，还有通过setTimeout/setInterval产生的异步调用。

原因： 在React的setState函数实现中，会根据一个变量isBatchingUpdates判断是直接更新this.state还是放到队列中回头再说，而isBatchingUpdates默认是false，也就表示setState会同步更新this.state，但是，有一个函数batchedUpdates，这个函数会把isBatchingUpdates修改为true，而当React在调用事件处理函数之前就会调用这个batchedUpdates，造成的后果，就是由React控制的事件处理过程setState不会同步更新this.state。

注意： setState的“异步”并不是说内部由异步代码实现，其实本身执行的过程和代码都是同步的，只是合成事件和钩子函数的调用顺序在更新之前，导致在合成事件和钩子函数中没法立马拿到更新后的值，形式了所谓的“异步”，当然可以通过第二个参数 setState(partialState, callback) 中的callback拿到更新后的结果。

综上，setState 只在合成事件和 hook() 中是“异步”的，在 原生事件和 setTimeout 中都是同步的。https://link.juejin.cn/?target=https%3A%2F%2Fgithub.com%2Fsteadicat%2Feslint-plugin-react-memo%3Futm_source%3Dttalk.im%26utm_medium%3Dwebsite%26utm_campaign%3DTech%2520Talk)