import { defineConfig } from 'windicss/helpers'
export default defineConfig({
  attributify: true,
  safelist: ['cursor-wait', 'pointer-events-none', 'text-red-400', 'text-yellow-400']
})
