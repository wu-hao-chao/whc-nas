/* eslint-disable @typescript-eslint/no-var-requires */
const Components = require('unplugin-vue-components/webpack')
const { NaiveUiResolver } = require('unplugin-vue-components/resolvers')
const path = require('path')

module.exports = {
	outputDir: '../server/src/public',
	configureWebpack: {
		plugins: [
			Components({
				resolvers: [NaiveUiResolver()]
			})
		],
		resolve: {
			alias: {
				'@global': path.resolve(__dirname, '../global'),
				'vue-i18n': 'vue-i18n/dist/vue-i18n.cjs.js'
			},
			aliasFields: []
		}
	},
	pluginOptions: {
		windicss: {},
		electronBuilder: {
			nodeIntegration: true,
			builderOptions: {
				productName: 'Nas',
				directories: {
					output: '../server/src/public/static/download/nas'
				},
				win: {
					icon: './public/favicon.ico',
					target: [{ target: 'nsis' }]
				},
				nsis: {
					oneClick: false, // 是否一键安装
					allowToChangeInstallationDirectory: true, // 是否允许用户更改创建目录
					installerIcon: './public/favicon.ico', // 安装程序图标
					installerHeaderIcon: './public/favicon.ico', // 安装时头部图标
					uninstallerIcon: './public/favicon.ico', // 卸载程序图标
					shortcutName: 'Nas', // 图标名称
					createDesktopShortcut: true, // 创建桌面快捷键
					createStartMenuShortcut: true // 创建开始栏快捷键
				},
				publish: [
					{
						provider: 'generic',
						url: process.env.VUE_APP_UPDATE_URL
					}
				]
			}
		}
	}
}
