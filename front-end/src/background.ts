'use strict'
import path from 'path'
import electron, { app, protocol, BrowserWindow, ipcMain, Tray, Menu } from 'electron'
import { createProtocol } from 'vue-cli-plugin-electron-builder/lib'
import { dialog } from './electron/tips'
import checkUpdate from './electron/checkUpdate'
// import installExtension, { VUEJS3_DEVTOOLS } from 'electron-devtools-installer'
const isDevelopment = process.env.NODE_ENV !== 'production'

// Scheme must be registered before the app is ready
protocol.registerSchemesAsPrivileged([{ scheme: 'app', privileges: { secure: true, standard: true } }])

// 系统托盘
let tray: Tray
// 主窗口
let mainWin: BrowserWindow

ipcMain.on('postMessage', (e, callback: string | Function, id: string) => {
  eval('callback = ' + callback)
  const res = (callback as Function)({ ...electron, mainWin })
  mainWin.webContents.send(id, res)
})

ipcMain.on('exit-app', () => app.exit())

async function createWindow() {
  mainWin = new BrowserWindow({
    width: 1000,
    height: 650,
    frame: false,
    resizable: false,
    transparent: true,
    show: true,
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false,
      webSecurity: false
    }
  })

  if (process.env.WEBPACK_DEV_SERVER_URL) {
    // Load the url of the dev server if in development mode
    await mainWin.loadURL(process.env.WEBPACK_DEV_SERVER_URL as string)
    // if (!process.env.IS_TEST) win.webContents.openDevTools()
  } else {
    createProtocol('app')
    // Load the index.html when not in development
    mainWin.loadURL('app://./index.html')
  }
}

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (BrowserWindow.getAllWindows().length === 0) createWindow()
})

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', async () => {
  // if (isDevelopment && !process.env.IS_TEST) {
  //   // Install Vue Devtools
  //   try {
  //     await installExtension(VUEJS3_DEVTOOLS)
  //   } catch (e: any) {
  //     console.error('Vue Devtools failed to install:', e.toString())
  //   }
  // }
  createWindow()
  checkUpdate(mainWin)

  tray = new Tray(path.join(__dirname, '../public/favicon.ico'))
  tray.setToolTip('Nas')
  const contextMenu = Menu.buildFromTemplate([{ label: '退出程序', click: () => app.exit() }])
  tray.setContextMenu(contextMenu)
  tray.on('double-click', () => {
    mainWin.isVisible() ? mainWin.hide() : mainWin.show()
    mainWin.isVisible() ? mainWin.setSkipTaskbar(false) : mainWin.setSkipTaskbar(true)
  })
})

// Exit cleanly on request from parent process in development mode.
if (isDevelopment) {
  if (process.platform === 'win32') {
    process.on('message', data => {
      if (data === 'graceful-exit') {
        app.quit()
      }
    })
  } else {
    process.on('SIGTERM', () => {
      app.quit()
    })
  }
}
