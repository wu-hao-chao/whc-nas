import axios from 'axios'

declare module 'axios' {
  export default axios
  export interface AxiosResponse {
    retainCode?: boolean
    loading?: boolean
  }
  export interface AxiosRequestConfig {
    retainCode?: boolean
    loading?: boolean
  }
}
