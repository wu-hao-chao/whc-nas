import { IS_ELECTRON } from '@/store/useEnv'
import useUserInfo from '@/store/useUserInfo'
import { $postMessage } from '@/utils/ipcRenderer'
import { IpcRenderer } from 'electron'

let ipcRenderer: IpcRenderer
if (IS_ELECTRON) ipcRenderer = require('electron').ipcRenderer

export default () => {
  if (!IS_ELECTRON) return
  ipcRenderer.on('set-save-download-file-path', (_, saveDownloadFilePath) => {
    useUserInfo().$patch({ saveDownloadFilePath })
  })
  $postMessage(({ dialog, mainWin }) => {
    const pathList = dialog.showOpenDialogSync({
      properties: ['openDirectory']
    })
    if (pathList && pathList[0]) {
      mainWin.webContents.send('set-save-download-file-path', pathList[0])
    }
  })
}
