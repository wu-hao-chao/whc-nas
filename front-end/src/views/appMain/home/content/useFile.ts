import { useI18n } from 'vue-i18n'
import { FileType, NasFile } from '@global/types'
import { reactive, toRefs, watch } from 'vue'
import { useRoute, useRouter } from 'vue-router'
import { useMessage } from '@/main'
import { useEnv } from '@/store/useEnv'
import { useMusic } from '@/store/useMusic'
import useUserInfo from '@/store/useUserInfo'

export default () => {
  const route = useRoute()
  const router = useRouter()
  const { t } = useI18n()
  const env = useEnv()
  const music = useMusic()

  const state = reactive({
    /** 选中的文件列表 */
    selectFileList: [] as NasFile[],
    /** 预览对象 */
    preview: {
      fileName: '',
      video: { show: false, url: '' },
      document: { show: false, url: '' }
    }
  })

  watch(route, () => (state.selectFileList = []))

  /** 获取文件缩略图 */
  const requireImg = (imgName: string) => require(`@/assets/img/${imgName}.png`)
  const getFileThumb = (file: NasFile) => {
    if (file.thumbnailExt) return `${env.nasThumbUrl}/${file.id}${file.thumbnailExt}`
    switch (file.fileType) {
      case FileType.dir:
        if (file.hasChild) return requireImg('all')
        return requireImg('dir')
      case FileType.image:
        return requireImg('img')
      case FileType.audio:
        return requireImg('audio')
      case FileType.video:
        return requireImg('video')
      case FileType.archive:
        return requireImg('zip')
      case FileType.document:
        return requireImg('doc')
      case FileType.package:
        return requireImg('exe')
      default:
        return requireImg('other')
    }
  }

  /** 单击选择文件 */
  const clickFileHandler = (file: NasFile) => {
    const isSelect = state.selectFileList.find(item => item.id === file.id)
    if (isSelect) state.selectFileList = state.selectFileList.filter(item => item.id !== isSelect.id)
    else state.selectFileList.push(file)
  }

  /** 双击文件事件 */
  const dobuleClickFileHandler = (file: NasFile) => {
    state.preview.fileName = file.fileName
    let filePath = encodeURI(`${env.nasFileUrl}/${file.filePath}`)
    switch (file.fileType) {
      // 进入文件夹
      case FileType.dir:
        router.replace({
          query: { parentPath: file.filePath }
        })
        break
      // 预览图片
      case FileType.image: {
        const el = document.querySelector(`#previewImg${file.id} img`) as HTMLElement
        el && el.click()
        break
      }
      // 预览视频
      case FileType.video:
        state.preview.video.url = filePath
        state.preview.video.show = true
        break
      // 选中音频并打开播放器
      case FileType.audio:
        music.selectMusic(file.id)
        break
      // 预览文档
      case FileType.document: {
        const extMatch = file.fileName.match(/[^.]+$/)
        const ext = extMatch ? extMatch[0] : ''
        if (ext === 'rtf') return useMessage().warning(t('no-support-preview-document'))
        // 由于 office365 不支持预览 IPV6 地址的文档（对方ipv6服务器在美国），所以预览文档使用 ipv4 的文件地址
        if (env.isIpv6) {
          filePath = encodeURI(`${env.ipv4Origin}/static/users/${useUserInfo().id}/nas/file/${file.filePath}`)
        }
        if (['doc', 'docx', 'xls', 'xlsx', 'ppt', 'pptx'].includes(ext)) {
          filePath = 'https://view.officeapps.live.com/op/view.aspx?src=' + filePath
        }
        state.preview.document.url = filePath
        state.preview.document.show = true
        break
      }
    }
  }

  return {
    ...toRefs(state),

    /** 获取文件的缩略图 */
    getFileThumb,
    /** 单击文件事件 */
    clickFileHandler,
    /** 双击文件事件 */
    dobuleClickFileHandler
  }
}
