import dayjs from '@/utils/dayjs'
import { GetNasFileListResponse, NasFile } from '@global/types'
import { onMounted, onUnmounted, reactive, Ref, toRefs } from 'vue'

type Props = {
  fileData: Ref<GetNasFileListResponse>
  selectFileList: Ref<NasFile[]>
}

export default ({ fileData, selectFileList }: Props) => {
  const state = reactive({
    /** 框选矩形 */
    selectRect: {
      show: false,
      startPoint: { x: 0, y: 0, clickTime: dayjs().valueOf() },
      style: {} as any
    }
  })

  /** 开始框选设置最开始点击的位置 */
  const startRectSelect = (e: MouseEvent) => {
    // 如果点击 naive 组件，不触发矩选
    try {
      const matchNaiveClass = (e.target as HTMLElement).className.match(/^n-.+/g)
      if (matchNaiveClass) return
      // eslint-disable-next-line no-empty
    } catch (err) {}

    // 如果短时间内双击，不触发矩选
    const { x: lastX, y: lastY, clickTime: lastClickTime } = state.selectRect.startPoint
    if (e.clientX === lastX && e.clientY === lastY) {
      if (dayjs().valueOf() - lastClickTime < 500) return
    }

    state.selectRect.show = true
    state.selectRect.style = { top: '-999px', left: '-999px' }
    state.selectRect.startPoint = {
      x: e.pageX,
      y: e.pageY,
      clickTime: dayjs().valueOf()
    }
  }
  /** 框选移动设置框选的样式 */
  const moveRectSelect = (e: MouseEvent) => {
    if (!state.selectRect.show) return
    // 设置框选样式
    const { x: startX, y: startY } = state.selectRect.startPoint
    const { pageX: nowX, pageY: nowY } = e
    if (startX === nowX && startY === startY) return
    const rectWidth = Math.abs(startX - nowX)
    const rectHeight = Math.abs(startY - nowY)
    const rectLeft = startX < nowX ? startX : nowX
    const rectTop = startY < nowY ? startY : nowY
    state.selectRect.style = {
      width: rectWidth + 'px',
      height: rectHeight + 'px',
      left: rectLeft + 1 + 'px',
      top: rectTop + 1 + 'px'
    }

    const rectRight = rectWidth + rectLeft
    const rectBottom = rectHeight + rectTop
    // 选中文件
    ;[...document.querySelectorAll('[file-id]')].forEach(item => {
      const { left: fileLeft, top: fileTop, right: fileRight, bottom: fileBottom } = item.getClientRects()[0]
      const fileId = +(item.getAttribute('file-id') + '')
      if (
        //
        fileBottom >= rectTop &&
        fileRight >= rectLeft &&
        rectBottom >= fileTop &&
        rectRight >= fileLeft
      ) {
        if (selectFileList.value.find(item => item.id === fileId)) return
        const newFile = fileData.value.list.find(item => item.id === fileId)
        newFile && selectFileList.value.push(newFile)
      } else {
        const delIndex = selectFileList.value.findIndex(item => item.id === fileId)
        if (delIndex !== -1) selectFileList.value.splice(delIndex, delIndex + 1)
      }
    })
  }
  /** 框选结束获取选中的文件 */
  const endRectSelect = () => (state.selectRect.show = false)
  onMounted(() => {
    document.body.addEventListener('mousedown', startRectSelect)
    document.body.addEventListener('mousemove', moveRectSelect)
    document.body.addEventListener('mouseup', endRectSelect)
  })
  onUnmounted(() => {
    document.body.removeEventListener('mousedown', startRectSelect)
    document.body.removeEventListener('mousemove', moveRectSelect)
    document.body.removeEventListener('mouseup', endRectSelect)
  })

  return {
    ...toRefs(state)
  }
}
