import { $postMessage } from '@/utils/ipcRenderer'
import { reactive, toRefs } from 'vue'

export default () => {
  const state = reactive({
    isMaximize: false
  })

  const minimize = () => $postMessage(({ mainWin }) => mainWin.minimize())

  const maximize = async () => {
    if (!state.isMaximize) {
      $postMessage(({ mainWin, screen }) => {
        const { width, height } = screen.getPrimaryDisplay().workAreaSize
        mainWin.setBounds({ width, height, x: 0, y: 0 }, true)
      })
    } else {
      $postMessage(({ mainWin, screen }) => {
        const { width, height } = screen.getPrimaryDisplay().workAreaSize
        mainWin.setBounds({
          x: (width - 1000) / 2,
          y: (height - 650) / 2,
          width: 1000,
          height: 650
        })
      })
    }
    state.isMaximize = !state.isMaximize
  }

  const close = () => $postMessage(({ mainWin }) => mainWin.close())
  return {
    ...toRefs(state),

    /** 窗口最小化 */
    minimize,
    /** 窗口最大化 */
    maximize,
    /** 退出程序 */
    close
  }
}
