import { BrowserWindow, ipcMain } from 'electron'
import { DialogOptions, MessageOptions } from 'naive-ui'

/** naive 对话框 */
export const dialog = (win: BrowserWindow, opts: DialogOptions) =>
  new Promise((resolve, reject) => {
    win.webContents.send('dialog', opts)
    ipcMain.once('dialogRes', (e, res) => (res ? resolve(res) : reject(res)))
  })

/** naive 信息 */
export const message = (win: BrowserWindow, cont: string, opts: MessageOptions = {}) =>
  win.webContents.send('message', cont, opts)
