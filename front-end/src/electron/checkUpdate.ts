import { autoUpdater } from 'electron-updater'
import { BrowserWindow, app } from 'electron'
import { message, dialog } from './tips'
import { isEmpty } from 'lodash'

export default (win: BrowserWindow) => {
  // 在下载之前将autoUpdater的autoDownload属性设置成false，通过渲染进程触发主进程事件来实现这一设置(将自动更新设置成false)
  autoUpdater.autoDownload = false
  //设置版本更新地址，即将打包后的latest.yml文件和exe文件同时放在
  //http://xxxx/test/version/对应的服务器目录下,该地址和package.json的publish中的url保持一致
  autoUpdater.setFeedURL(process.env.VUE_APP_UPDATE_URL + '')
  // 当更新发生错误的时候触发。
  // autoUpdater.on('error', (err) => {
  //   message(win, JSON.stringify(err), { type: 'error' })
  // })
  // 当开始检查更新的时候触发
  // autoUpdater.on('checking-for-update', () => {
  //   message(win, '开始检查更新')
  // })
  // 发现可更新数据时
  autoUpdater.on('update-available', () => {
    autoUpdater.downloadUpdate()
  })
  // 没有可更新数据时
  autoUpdater.on('update-not-available', () => {
    win.webContents.send('download-progress', 100)
    setTimeout(() => win.webContents.send('update-finish'), 1000)
  })
  // 下载监听
  autoUpdater.on('download-progress', (progress) => {
    win.webContents.send('download-progress', progress.percent)
  })
  // 下载完成
  autoUpdater.on('update-downloaded', () => {
    message(win, '下载完成，开始安装程序')
    win.destroy()
    autoUpdater.quitAndInstall()
  })

  autoUpdater.checkForUpdates().catch((err) => {
    dialog(win, {
      type: 'error',
      title: '检查更新失败',
      content: isEmpty(err) ? '更新失败，即将退出程序' : JSON.stringify(err)
    }).finally(() => {
      win.destroy()
      app.quit()
    })
  })
}
