import { $rules } from './index'
declare module '@vue/runtime-core' {
  export interface ComponentCustomProperties {
    $rules: typeof $rules
  }
}
