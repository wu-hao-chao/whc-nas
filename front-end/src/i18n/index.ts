import { useApi } from './../api/index'
import { createI18n, useI18n } from 'vue-i18n'
import zhCN from './locales/zh-CN.json'
import enUS from './locales/en-US.json'
import { Ref, watch } from 'vue'
import useUserInfo from '@/store/useUserInfo'
import { getItem } from '@/utils/storage'

export default createI18n({
  locale: getItem('userInfo')?.locale || 'zhCN',
  messages: { zhCN, enUS }
})

export const useI18nOpts = () => {
  const locale = useI18n().locale as Ref<'zhCN' | 'enUS'>
  watch(locale, locale => {
    useUserInfo().$patch({ locale })
    useApi().post('/user/chang_locale', { locale })
  })
  return {
    locale: useI18n().locale,
    options: [
      { label: '简体中文', key: 'zhCN' },
      { label: 'English', key: 'enUS' }
    ]
  }
}
