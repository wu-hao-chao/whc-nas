import { useI18n } from 'vue-i18n'
import { zhCN, enUS, dateZhCN, dateEnUS } from 'naive-ui'
import { reactive, watch, toRefs } from 'vue'

const naiveI18nMsg = {
  zhCN,
  enUS,
  dateZhCN,
  dateEnUS
}

export default () => {
  const { locale } = useI18n()

  const opts = reactive({
    locale: naiveI18nMsg[locale.value],
    dateLocale: naiveI18nMsg['date' + locale.value.replace(/./, (v) => v.toUpperCase())]
  })
  watch(locale, () => {
    opts.locale = naiveI18nMsg[locale.value]
    opts.dateLocale = naiveI18nMsg['date' + locale.value]
  })
  return toRefs(opts)
}
