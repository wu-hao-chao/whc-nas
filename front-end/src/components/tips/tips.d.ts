import { useMessage, useDialog, useLoadingBar, useNotification } from 'naive-ui'

declare module '@vue/runtime-core' {
  const $message = useMessage()
  const $dialog = useDialog()
  const $loadingBar = useLoadingBar()
  const $notification = useNotification()
  export interface ComponentCustomProperties {
    $message: typeof $message
    $dialog: typeof $dialog
    $notification: typeof $notification
  }
}
