import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import 'windi.css'
import i18n from './i18n'
import { createPinia } from 'pinia'
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate'
import store from './store'
import utils from './utils'
import rules from './formRule'
import { MessageApiInjection } from 'naive-ui/es/message/src/MessageProvider'
import { DialogApiInjection } from 'naive-ui/es/dialog/src/DialogProvider'
import { NotificationApiInjection } from 'naive-ui/es/notification/src/NotificationProvider'
import { LoadingBarApiInjection } from 'naive-ui/es/loading-bar/src/LoadingBarProvider'

const pinia = createPinia()
pinia.use(piniaPluginPersistedstate)

const app = createApp(App)
app
  //
  .use(pinia)
  .use(router)
  .use(i18n)
  .use(store)
  .use(utils)
  .use(rules)
  .mount('#app')
export default app


const common = app.config.globalProperties
export const useMessage = () => common.$message as MessageApiInjection
export const useDialog = () => common.$dialog as DialogApiInjection
export const useNotification = () => common.$notification as NotificationApiInjection
export const useLoadingBar = () => common.$loadingBar as LoadingBarApiInjection
