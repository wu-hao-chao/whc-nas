import { useEnv, IS_ELECTRON, NODE_ENV } from '@/store/useEnv'
import $useUserInfo from './useUserInfo'

declare module '@vue/runtime-core' {
  export interface ComponentCustomProperties {
    $useUserInfo: typeof $useUserInfo
    $useEnv: typeof useEnv
    $IS_ELECTRON: typeof IS_ELECTRON
    $NODE_ENV: typeof NODE_ENV
  }
}
