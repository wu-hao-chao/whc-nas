import { App } from 'vue'
import $useUserInfo from './useUserInfo'
import { IS_ELECTRON, NODE_ENV, useEnv } from './useEnv'

export default {
  install: (app: App) =>
    Object.assign(app.config.globalProperties, {
      $useUserInfo,
      $useEnv: useEnv,
      $IS_ELECTRON: IS_ELECTRON,
      $NODE_ENV: NODE_ENV
    })
}
