import { useMessage } from '@/main'
import { MessageReactive } from 'naive-ui'

const HTML_El = document.querySelector('html') as HTMLElement
export default (() => {
  let message: MessageReactive | null
  return {
    add: (title?: string) => {
      HTML_El.classList.add('cursor-wait', 'pointer-events-none')
      if (!title) return
      message = useMessage().loading(title, { duration: 0 })
    },
    remove: () => {
      if (message) message.destroy()
      message = null
      HTML_El.classList.remove('cursor-wait', 'pointer-events-none')
    }
  }
})()
