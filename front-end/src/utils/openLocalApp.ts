import { useMessage } from '@/main'
import { IS_ELECTRON } from '@/store/useEnv'
import i18n from '@/i18n'

export default async (filePath: string) => {
  const t = i18n.global.t
  if (IS_ELECTRON) {
    const { exec } = await import('child_process')
    return exec(filePath, err => err && useMessage().error(JSON.stringify(err)))
  }
  useMessage().error(t('web-side-no-support-open-local-app'))
}
