import { App } from 'vue'
import _ from 'lodash'
import $loading from './loading'
import $dayjs, { formatTime } from './dayjs'
import $Reg from '@global/Reg'
import { $postMessage } from './ipcRenderer'

const utils = {
  _,
  $dayjs,
  $formatTime: formatTime,
  $Reg,
  $postMessage,
  $loading,
  $require: (imgName: string) => require('@/assets/img/' + imgName)
}

export default {
  install: (app: App) => {
    Object.assign(app.config.globalProperties, utils)
  }
}
