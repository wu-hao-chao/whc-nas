import { FileType } from '@global/types'

export const imgExts = [
  'bmp',
  'cur',
  'dds',
  'exr',
  'gif',
  'ico',
  'jpg',
  'jpeg',
  'pcx',
  'pbm',
  'pfm',
  'pgm',
  'ppm',
  'png',
  'psd',
  'svg',
  'tga',
  'tif',
  'tiff',
  'webp',
  'heic'
]

export const videoExts = [
  '3gp',
  '3g2',
  'asf',
  'avi',
  'f4f',
  'f4i',
  'f4m',
  'f4v',
  'flv',
  'h264',
  'm4v',
  'mkv',
  'mov',
  'mp4',
  'mpg',
  'ogg',
  'ogv',
  'qt',
  'rm',
  'rmvb',
  'vob',
  'webm',
  'wmv'
]

export const audioExts = [
  'aac',
  'aif',
  'ape',
  'flac',
  'oga',
  'opus',
  'm3u',
  'm4a',
  'mid',
  'midi',
  'mp3',
  'ra',
  'wav',
  'wma',
  'weba'
]

export const docExts = [
  'c',
  'cfg',
  'cpp',
  'cs',
  'css',
  'csv',
  'dat',
  'doc',
  'docx',
  'h',
  'hpp',
  'ini',
  'js',
  'log',
  'm',
  'pdf',
  'ppt',
  'pptx',
  'ps',
  'py',
  'rtf',
  'sh',
  'tex',
  'torrent',
  'txt',
  'xls',
  'xlsx'
]

export const archiveExts = [
  '7z',
  'bin',
  'cbr',
  'cue',
  'deb',
  'dmg',
  'gz',
  'iso',
  'mdf',
  'pkg',
  'rar',
  'rpm',
  'sitx',
  'tar.gz',
  'vcd',
  'zip'
]

export const appExts = ['apk', 'app', 'bat', 'cgi', 'com', 'dll', 'exe', 'gadget', 'jar', 'msi', 'pif', 'sys']

export const getFileType = (fileName: string): FileType => {
  fileName = fileName.toLowerCase()
  const match = fileName.match(/\.[^.]+$/)
  if (!match) return FileType.other
  const ext = match[0].replace('.', '')
  if (imgExts.includes(ext)) return FileType.image
  if (videoExts.includes(ext)) return FileType.video
  if (audioExts.includes(ext)) return FileType.audio
  if (docExts.includes(ext)) return FileType.document
  if (archiveExts.includes(ext)) return FileType.archive
  if (appExts.includes(ext)) return FileType.package
  return FileType.other
}
