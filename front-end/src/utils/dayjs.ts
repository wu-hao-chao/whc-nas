import dayjs from 'dayjs'
import isLeapYear from 'dayjs/plugin/isLeapYear'
import duration from 'dayjs/plugin/duration'
import relativeTime from 'dayjs/plugin/relativeTime'
import 'dayjs/locale/zh-cn'
import 'dayjs/locale/es-us'

dayjs.extend(isLeapYear)
dayjs.extend(relativeTime)
dayjs.extend(duration)
dayjs.locale('zh-cn')

export const formatTime = (time?: string | number | Date, format = 'YYYY-MM-DD HH:mm:ss') => dayjs(time).format(format)

export default dayjs
