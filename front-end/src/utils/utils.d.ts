import _ from 'lodash'
import $dayjs, { formatTime } from './dayjs'
import $Reg from '@global/Reg'
import { $postMessage } from './ipcRenderer'
import { $loading } from './loading'
declare module '@vue/runtime-core' {
  export interface ComponentCustomProperties {
    _: typeof _
    $dayjs: typeof $dayjs
    $formatTime: typeof formatTime
    $Reg: typeof $Reg
    $postMessage: typeof $postMessage
    $loading: typeof $loading
    $require: (imgName: string) => string
  }
}
