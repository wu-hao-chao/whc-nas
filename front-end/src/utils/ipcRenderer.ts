/* eslint-disable @typescript-eslint/no-empty-function */
import { IS_ELECTRON } from '@/store/useEnv'
import electron, { BrowserWindow } from 'electron'
import { uniqueId } from 'lodash'

type Electron = typeof electron

let postMessage: <T>(
  cb: (electron: Electron & { mainWin: BrowserWindow; mediaRecorderWin: BrowserWindow }) => T
) => Promise<T> = (async () => {}) as any

if (IS_ELECTRON) {
  const { ipcRenderer } = require('electron')
  postMessage = cb =>
    new Promise(resolve => {
      const id = uniqueId('electron-post')
      ipcRenderer.send('postMessage', cb.toString(), id)
      ipcRenderer.once(id, (e, value) => resolve(value))
    })
}
export const $postMessage = postMessage
