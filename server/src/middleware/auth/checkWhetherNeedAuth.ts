const whiteUrl = [
	//
	'/api/sms',
	'/api/user/check_phone',
	'/api/user/register',
	'/api/user/login_psd',
	'/api/user/login_sms'
]

const whiteUrlReg = [/^\/blog.*/]

export default (url: string) => {
	if (whiteUrl.includes(url)) return true
	return whiteUrlReg.some(reg => reg.test(url))
}
