import { Context, Next } from 'koa'
import db from '../../db'
export default async (ctx: Context, next: Next) => {
	ctx.db = {
		select: async query => await db(query),
		count: async query => {
			const res = await db(query)
			return res[0]['COUNT(*)']
		},
		selectOne: async query => {
			const res = await db(query + ' LIMIT 1')
			return res[0]
		},
		insert: async query => {
			const { insertId } = await db(query)
			return insertId
		},
		update: async query => {
			const { changedRows } = await db(query)
			return changedRows
		},
		delete: async query => {
			const { changedRows } = await db(query)
			return changedRows
		}
	}
	await next()
}
