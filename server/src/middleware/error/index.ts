import { Context, Next } from 'koa'
import { systemLogger } from '../../logs/logger'

export default async (ctx: Context, next: Next) => {
	try {
		await next()
	} catch (err: any) {
		systemLogger().error(err)
		return ctx.error({ code: 500, msg: err })
	}
}
