import { Context, Next } from 'koa'
import { isIPv6 } from 'net'

export default async (ctx: Context, next: Next) => {
	console.log(isIPv6(ctx.ip))
	next()
}
