import KoaRouter from 'koa-router'
import user from './user'
import sms from './sms'
import nas from './nas'

const router = new KoaRouter()
router
	//
	.use('/user', user.routes())
	.use('/sms', sms.routes())
	.use('/nas', nas.routes())

export default router
