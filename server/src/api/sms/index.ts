import KoaRouter from 'koa-router'
import logic from './logic'

export default new KoaRouter()
	//
	.post('/', logic.sendSms)
