import KoaRouter from 'koa-router'
import logic from './logic'

export default new KoaRouter()
	.get('/check_phone', logic.checkPhone) // 校验手机号是否注册
	.post('/register', logic.register) // 注册用户
	.post('/login_psd', logic.loginPsd) // 密码登录
	.post('/login_sms', logic.loginSms) // 手机验证码登录
	.post('/chang_locale', logic.changeLocale) // 修改语言环境
