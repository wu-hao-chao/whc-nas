import { Context } from 'koa'
import KoaRouter from 'koa-router'
import logic from './logic'

export default new KoaRouter()
	.get('/file', logic.getFile) // 根据 id 或者路径获取文件信息
	.get('/list', logic.getList) // 获取文件列表
	.get('/list_deep', logic.getListDeep) // 获取文件夹下所有文件和子文件夹
	.post('/upload', logic.uploadFile) // 上传文件
	.post('/create_dir', logic.createDir) // 创建目录
	.post('/del_files', logic.delFiles) // 删除文件
	.post('/rename', logic.renameFile) // 重命名文件
	.post('/copy_files', logic.copyFiles) // 复制文件
