-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- 主机： localhost
-- 生成日期： 2023-03-10 09:01:44
-- 服务器版本： 5.7.26
-- PHP 版本： 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 数据库： `pi`
--

-- --------------------------------------------------------

--
-- 表的结构 `login`
--

CREATE TABLE `login` (
  `id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `phone` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `userIp` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `userAgent` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `isVaild` tinyint(1) NOT NULL DEFAULT '1',
  `createTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 转存表中的数据 `login`
--

INSERT INTO `login` (`id`, `uid`, `phone`, `userIp`, `userAgent`, `token`, `isVaild`, `createTime`, `updateTime`) VALUES
(1, 1, '13790867170', '::ffff:127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.114 Safari/537.36 Edg/103.0.1264.49', '910c6e51b5a793deaab216ac131e4e5c', 0, '2022-07-10 02:11:10', '2022-07-13 15:47:40'),
(2, 2, '13790867170', '::ffff:127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.114 Safari/537.36 Edg/103.0.1264.49', '4ab84301a1b602db52042893641ba62c', 0, '2022-07-13 15:47:40', '2022-07-15 05:12:43'),
(3, 2, '13790867170', '::ffff:127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.114 Safari/537.36 Edg/103.0.1264.49', 'ec1838981986a18901d34ae5f5a9de3c', 0, '2022-07-15 05:12:43', '2022-07-15 12:07:41'),
(4, 2, '13790867170', '::ffff:127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.114 Safari/537.36 Edg/103.0.1264.49', '5a0659a685b3cf82fad9ca3596fa1f72', 0, '2022-07-15 12:07:41', '2022-07-15 14:41:52'),
(5, 2, '13790867170', '2409:8a55:4418:54a0:95a6:14b0:1bd1:ebf7', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) front-end/0.0.10 Chrome/91.0.4472.164 Electron/13.6.9 Safari/537.36', '8b92e6d5d720b10bcc83c08edba14f89', 0, '2022-07-15 14:41:52', '2022-07-15 17:32:38'),
(6, 2, '13790867170', '2409:8a55:4418:54a0:95a6:14b0:1bd1:ebf7', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) front-end/0.0.10 Chrome/91.0.4472.164 Electron/13.6.9 Safari/537.36', '68d4e19add13a07e62983ac0bb9c9d44', 0, '2022-07-15 17:32:38', '2022-07-15 18:36:39'),
(7, 2, '13790867170', '::ffff:127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.114 Safari/537.36 Edg/103.0.1264.49', '985e4f640f699b1e3d195f11f824e284', 0, '2022-07-15 18:36:39', '2022-07-15 18:37:21'),
(8, 2, '13790867170', '2409:8a55:4418:54a0:95a6:14b0:1bd1:ebf7', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.114 Safari/537.36 Edg/103.0.1264.49', '69d1e51b7c00cdb4fbb162c45d6547d4', 0, '2022-07-15 18:37:21', '2022-07-16 03:21:35'),
(9, 2, '13790867170', '2409:8a55:441c:7300:7a:bc0f:b60a:5c99', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) front-end/0.0.10 Chrome/91.0.4472.164 Electron/13.6.9 Safari/537.36', 'd12673af2888c9be42be68d07ebd9805', 0, '2022-07-16 03:21:35', '2022-07-16 17:55:54'),
(10, 2, '13790867170', '2409:8a55:441c:7300:7a:bc0f:b60a:5c99', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.114 Safari/537.36 Edg/103.0.1264.49', '238b5f89da346899b337728bd75d00a7', 0, '2022-07-16 17:55:54', '2022-07-16 17:56:54'),
(11, 2, '13790867170', '2409:8a55:441c:7300:7a:bc0f:b60a:5c99', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.114 Safari/537.36 Edg/103.0.1264.49', 'b27ffad9ebb7948bd1c544b723767dac', 0, '2022-07-16 17:56:54', '2022-07-16 18:00:19'),
(12, 2, '13790867170', '::ffff:127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.114 Safari/537.36 Edg/103.0.1264.49', '56f760fe7c2bd274a6c68d09082df7e8', 0, '2022-07-16 18:00:19', '2022-07-16 18:00:37'),
(13, 2, '13790867170', '2409:8a55:441c:7300:7a:bc0f:b60a:5c99', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.114 Safari/537.36 Edg/103.0.1264.49', 'c78893452a386f51909e82fdf6ca306a', 0, '2022-07-16 18:00:37', '2022-07-16 18:10:32'),
(14, 2, '13790867170', '2409:8a55:441c:7300:7a:bc0f:b60a:5c99', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.114 Safari/537.36 Edg/103.0.1264.49', '7bb0cc0692ee2366748ca309383c4b53', 0, '2022-07-16 18:10:32', '2022-07-16 18:11:20'),
(15, 2, '13790867170', '2409:8a55:441c:7300:7a:bc0f:b60a:5c99', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.114 Safari/537.36 Edg/103.0.1264.49', 'f1f71d58031fefe69c4f2daea7b203f4', 0, '2022-07-16 18:11:20', '2022-07-16 18:17:54'),
(16, 2, '13790867170', '2409:8a55:441c:7300:7a:bc0f:b60a:5c99', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.114 Safari/537.36 Edg/103.0.1264.49', 'f0aaab68cff4f9e84e6722294b47fbba', 0, '2022-07-16 18:17:54', '2022-07-16 19:16:24'),
(17, 2, '13790867170', '2409:8a55:441c:7300:7a:bc0f:b60a:5c99', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.114 Safari/537.36 Edg/103.0.1264.49', 'edf033dbb5ec33e9eb6b23110bf4c1f9', 0, '2022-07-16 19:16:24', '2022-07-16 19:22:05'),
(18, 2, '13790867170', '2409:8a55:441c:7300:7a:bc0f:b60a:5c99', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.114 Safari/537.36 Edg/103.0.1264.49', '5122b6060964b131beb02e53bc5282af', 0, '2022-07-16 19:22:05', '2022-07-16 19:36:51'),
(19, 2, '13790867170', '2409:8a55:441c:7300:7a:bc0f:b60a:5c99', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.114 Safari/537.36 Edg/103.0.1264.49', '219ba372d49655c606288aafe63c501d', 0, '2022-07-16 19:36:51', '2022-07-16 19:47:23'),
(20, 2, '13790867170', '2409:8a55:441c:7300:7a:bc0f:b60a:5c99', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.114 Safari/537.36 Edg/103.0.1264.49', '4b3c12f103c89bd571b0df76ad3ba124', 0, '2022-07-16 19:47:23', '2022-07-16 20:20:53'),
(21, 2, '13790867170', '::ffff:127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.114 Safari/537.36 Edg/103.0.1264.49', '72499e901d75ebf19f6e758bb8722460', 0, '2022-07-16 20:20:53', '2022-07-16 20:21:21'),
(22, 2, '13790867170', '2409:8a55:441c:7300:7a:bc0f:b60a:5c99', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.114 Safari/537.36 Edg/103.0.1264.49', 'a156b69d3f77acb9a9ea8bba0f1ce550', 0, '2022-07-16 20:21:21', '2022-07-17 06:15:34'),
(23, 2, '13790867170', '2409:8a55:441c:7300:5133:5022:fcd2:f0a7', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.114 Safari/537.36 Edg/103.0.1264.49', 'a425b40b0448acd0d2a3642f0cf5d1de', 0, '2022-07-17 06:15:34', '2022-07-17 06:35:32'),
(24, 2, '13790867170', '2409:8a55:441c:7300:5133:5022:fcd2:f0a7', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.114 Safari/537.36 Edg/103.0.1264.49', '26ef99b6a1f55acd2d3c4f85fc8c431a', 0, '2022-07-17 06:35:32', '2022-07-17 09:22:51'),
(25, 2, '13790867170', '2409:8a55:441c:7300:5133:5022:fcd2:f0a7', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.114 Safari/537.36 Edg/103.0.1264.49', 'ac1c7d6b2ce7c796500a32a49a3ab539', 0, '2022-07-17 09:22:51', '2022-07-17 15:32:36'),
(26, 2, '13790867170', '2409:8a55:441c:7300:8cc1:37db:9474:cebe', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.114 Safari/537.36 Edg/103.0.1264.62', 'f01ad2984f83ca6d0f862e528998a56a', 0, '2022-07-17 15:32:36', '2022-07-17 15:47:01'),
(27, 2, '13790867170', '::ffff:127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36 NetType/WIFI MicroMessenger/7.0.20.1781(0x6700143B) WindowsWechat(0x6307001d)', '3058d6645b2ad819f8a2600931bf55d8', 0, '2022-07-17 15:47:01', '2022-07-17 15:55:16'),
(28, 2, '13790867170', '2409:8a55:441c:7300:8cc1:37db:9474:cebe', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.114 Safari/537.36 Edg/103.0.1264.62', '882c69a2f397406ccfec618f957ba9ca', 0, '2022-07-17 15:55:16', '2022-07-29 07:19:54'),
(29, 2, '13790867170', '::ffff:127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.134 Safari/537.36 Edg/103.0.1264.71', 'e87941a26def4658cb45fa154c83e556', 0, '2022-07-29 07:19:54', '2022-08-01 09:43:58'),
(30, 2, '13790867170', '2409:8a55:4417:7720:745e:ac1c:e85b:2f31', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.134 Safari/537.36 Edg/103.0.1264.77', 'cea5936506db5f970f41e22fe97d0d5d', 0, '2022-08-01 09:43:58', '2022-08-01 09:43:58'),
(31, 2, '13790867170', '2409:8a55:4417:7720:745e:ac1c:e85b:2f31', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.134 Safari/537.36 Edg/103.0.1264.77', 'cea5936506db5f970f41e22fe97d0d5d', 0, '2022-08-01 09:43:58', '2022-08-01 09:44:34'),
(32, 2, '13790867170', '2409:8a55:4417:7720:745e:ac1c:e85b:2f31', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.134 Safari/537.36 Edg/103.0.1264.77', '414a2e1d074835465ae76a8dafd457e2', 0, '2022-08-01 09:44:34', '2022-08-01 09:50:34'),
(33, 2, '13790867170', '2409:8a55:4417:7720:745e:ac1c:e85b:2f31', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.134 Safari/537.36 Edg/103.0.1264.77', '1d05dfbe1c9196f54dcbe578ae2c0e61', 0, '2022-08-01 09:50:34', '2022-08-12 03:06:23'),
(34, 2, '13790867170', '::ffff:127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.5112.81 Safari/537.36 Edg/104.0.1293.47', '4bf1fb905d2e49d50f025f7c8ba36367', 0, '2022-08-12 03:06:23', '2022-08-15 14:21:12'),
(35, 2, '13790867170', '::ffff:127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.5112.81 Safari/537.36 Edg/104.0.1293.54', '01d6dbf46fffeca8bfe1b84df77cafcf', 0, '2022-08-15 14:21:12', '2022-08-28 19:54:36'),
(36, 2, '13790867170', '2409:8a55:441c:2ae0:9840:adc0:6c88:ec2d', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.5112.102 Safari/537.36 Edg/104.0.1293.70', 'a62c5f9df63e33e382c3afe3df27a6ab', 0, '2022-08-28 19:54:36', '2022-09-13 17:58:26'),
(42, 2, '13790867170', '2409:8a55:4414:ff10:982b:245f:9aca:a734', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) front-end/0.0.12 Chrome/91.0.4472.164 Electron/13.6.9 Safari/537.36', '13d48eb9faa1a77c4f05f63cf49e47f9', 0, '2022-09-13 17:58:26', '2022-09-13 18:06:38'),
(43, 2, '13790867170', '2409:8a55:4414:ff10:982b:245f:9aca:a734', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) front-end/0.0.13 Chrome/91.0.4472.164 Electron/13.6.9 Safari/537.36', 'f530d1ab2dcf159568d905ed17a9a508', 0, '2022-09-13 18:06:38', '2022-09-13 18:38:59'),
(44, 2, '13790867170', '2409:8a55:4414:ff10:982b:245f:9aca:a734', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) front-end/0.0.12 Chrome/91.0.4472.164 Electron/13.6.9 Safari/537.36', 'afbca80f0446c54239bb0f2c4d4cfdcc', 0, '2022-09-13 18:38:59', '2022-09-14 12:33:08'),
(45, 2, '13790867170', '2409:8a55:4414:ff10:982b:245f:9aca:a734', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/105.0.0.0 Safari/537.36 Edg/105.0.1343.33', '99a997235440946ef6ec40e0ed886cec', 0, '2022-09-14 12:33:08', '2022-09-14 12:48:45'),
(46, 2, '13790867170', '2409:8a55:4414:ff10:982b:245f:9aca:a734', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) front-end/0.0.14 Chrome/91.0.4472.164 Electron/13.6.9 Safari/537.36', 'bedfe5e26623606d865aa9ea56498d38', 0, '2022-09-14 12:48:45', '2022-09-14 14:02:24'),
(47, 2, '13790867170', '::ffff:127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) front-end/0.0.14 Chrome/91.0.4472.164 Electron/13.6.9 Safari/537.36', 'f25b14ea4bcccee78b55f4f0683e9e4a', 0, '2022-09-14 14:02:24', '2022-09-15 12:15:48'),
(48, 2, '13790867170', '2409:8a55:441f:4d00:1d79:843e:18ff:38ad', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/105.0.0.0 Safari/537.36 Edg/105.0.1343.33', 'd69f82812732cf9580527d829384d6fe', 0, '2022-09-15 12:15:48', '2022-09-15 12:18:26'),
(49, 2, '13790867170', '2409:8a55:441f:4d00:39cd:a58a:b44a:2743', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.5112.102 Safari/537.36 Edg/104.0.1293.70', '24dd58b9563015df4bc7186e11e3ba39', 0, '2022-09-15 12:18:26', '2022-09-15 12:19:00'),
(50, 2, '13790867170', '2409:8a55:441f:4d00:39cd:a58a:b44a:2743', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.5112.102 Safari/537.36 Edg/104.0.1293.70', '117082fff25597ac1e5f25da6ea5cbfa', 0, '2022-09-15 12:19:00', '2022-09-15 12:38:00'),
(51, 2, '13790867170', '2409:8a55:441f:4d00:1d79:843e:18ff:38ad', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/105.0.0.0 Safari/537.36 Edg/105.0.1343.33', '27d6229c9afdd2e8688ef20e6a68a7d0', 0, '2022-09-15 12:38:00', '2022-09-15 13:21:51'),
(52, 2, '13790867170', '2409:8a55:441f:4d00:39cd:a58a:b44a:2743', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.5112.102 Safari/537.36 Edg/104.0.1293.70', 'a8ac2a238ab7a9a7b27fdb7ed4448cf2', 0, '2022-09-15 13:21:51', '2022-09-15 13:40:23'),
(53, 2, '13790867170', '2409:8a55:441f:4d00:1d79:843e:18ff:38ad', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/105.0.0.0 Safari/537.36 Edg/105.0.1343.33', '4c29f579e15d85ffcdc452a63384b828', 0, '2022-09-15 13:40:23', '2022-09-15 13:41:08'),
(54, 2, '13790867170', '2409:8a55:441f:4d00:39cd:a58a:b44a:2743', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.5112.102 Safari/537.36 Edg/104.0.1293.70', '0a7e482d486f733bdfed3af62a741eba', 0, '2022-09-15 13:41:08', '2022-09-15 13:50:19'),
(55, 2, '13790867170', '2409:8a55:441f:4d00:1d79:843e:18ff:38ad', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/105.0.0.0 Safari/537.36 Edg/105.0.1343.33', '5d08dd686882cb28520c18f7338ec894', 0, '2022-09-15 13:50:19', '2022-09-15 13:50:45'),
(56, 2, '13790867170', '2409:8a55:441f:4d00:39cd:a58a:b44a:2743', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.5112.102 Safari/537.36 Edg/104.0.1293.70', '1b5555b89bb5a3b8a4ec885215ed5cca', 0, '2022-09-15 13:50:45', '2022-09-15 13:53:57'),
(57, 2, '13790867170', '2409:8a55:441f:4d00:1d79:843e:18ff:38ad', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/105.0.0.0 Safari/537.36 Edg/105.0.1343.33', '8baed08aea807f5fec02b6fce2359177', 0, '2022-09-15 13:53:57', '2022-09-15 13:54:48'),
(58, 2, '13790867170', '2409:8a55:441f:4d00:39cd:a58a:b44a:2743', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.5112.102 Safari/537.36 Edg/104.0.1293.70', '094b325f66488df5671ad9f5bb910f6c', 0, '2022-09-15 13:54:48', '2022-09-15 13:56:52'),
(59, 2, '13790867170', '2409:8a55:441f:4d00:1d79:843e:18ff:38ad', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/105.0.0.0 Safari/537.36 Edg/105.0.1343.33', '41dafcea732cab50013f5ee9121172fe', 0, '2022-09-15 13:56:52', '2022-09-15 14:12:21'),
(60, 2, '13790867170', '2409:8a55:441f:4d00:39cd:a58a:b44a:2743', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.5112.102 Safari/537.36 Edg/104.0.1293.70', 'd40888a52b7c08e166342685e6565864', 0, '2022-09-15 14:12:21', '2022-09-15 16:47:50'),
(61, 2, '13790867170', '::ffff:127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) front-end/0.0.11 Chrome/91.0.4472.164 Electron/13.6.9 Safari/537.36', 'c5d369d6038846ec83adc5243688f1ca', 0, '2022-09-15 16:47:50', '2022-09-16 15:34:13'),
(62, 2, '13790867170', '::ffff:127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.5112.102 Safari/537.36 Edg/104.0.1293.70', '60510d69dd61259dfc17ecc51aa76a28', 1, '2022-09-16 15:34:13', '2022-09-16 15:34:13');

-- --------------------------------------------------------

--
-- 表的结构 `nas_files`
--

CREATE TABLE `nas_files` (
  `id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `fileName` char(255) COLLATE utf8_unicode_ci NOT NULL,
  `parentPath` text COLLATE utf8_unicode_ci NOT NULL,
  `filePath` text COLLATE utf8_unicode_ci NOT NULL,
  `fileSize` bigint(20) DEFAULT NULL,
  `fileType` tinyint(4) NOT NULL,
  `uploaded` bigint(20) DEFAULT NULL,
  `thumbnailExt` char(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uploadFinish` tinyint(1) NOT NULL DEFAULT '0',
  `createTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- 表的结构 `sms`
--

CREATE TABLE `sms` (
  `id` int(11) NOT NULL,
  `phone` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `type` tinyint(4) NOT NULL,
  `isVaild` tinyint(1) NOT NULL DEFAULT '1',
  `createTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 转存表中的数据 `sms`
--

INSERT INTO `sms` (`id`, `phone`, `type`, `isVaild`, `createTime`, `updateTime`) VALUES
(1, '13790867170', 0, 0, '2022-07-10 02:08:54', '2022-07-10 02:09:17'),
(2, '13790867170', 0, 0, '2022-07-13 15:46:46', '2022-07-13 15:47:31');

-- --------------------------------------------------------

--
-- 表的结构 `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `phone` varchar(11) NOT NULL,
  `name` varchar(12) NOT NULL,
  `psd` varchar(12) NOT NULL,
  `locale` varchar(10) NOT NULL DEFAULT 'zhCN',
  `createTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `user`
--

INSERT INTO `user` (`id`, `phone`, `name`, `psd`, `locale`, `createTime`, `updateTime`) VALUES
(2, '13790867170', 'whc666', '123123', 'zhCN', '2022-07-13 15:47:31', '2022-09-15 16:52:07');

--
-- 转储表的索引
--

--
-- 表的索引 `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `nas_files`
--
ALTER TABLE `nas_files`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `sms`
--
ALTER TABLE `sms`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `phone` (`phone`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `login`
--
ALTER TABLE `login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- 使用表AUTO_INCREMENT `nas_files`
--
ALTER TABLE `nas_files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19274;

--
-- 使用表AUTO_INCREMENT `sms`
--
ALTER TABLE `sms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- 使用表AUTO_INCREMENT `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
