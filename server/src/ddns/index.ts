// This file is auto-generated, don't edit it
import Alidns20150109, * as $Alidns20150109 from '@alicloud/alidns20150109'
// 依赖的模块可通过下载工程中的模块依赖文件或右上角的获取 SDK 依赖信息查看
import OpenApi, * as $OpenApi from '@alicloud/openapi-client'
import Util, * as $Util from '@alicloud/tea-util'
import * as $tea from '@alicloud/tea-typescript'
import { networkInterfaces } from 'os'
import { logger } from '../logs/logger'
import env from '../env'
import _ from 'lodash'

const DOMAIN_NAME = env.DOMAIN_NAME
const accessKeyId = 'LTAI5tHK56sYfFG7uxvXojTU'
const accessKeySecret = 'WgBUgcZsgJ6zxvZiYrWe4wdmleZdSJ'

export default class Client {
	/**
	 * 使用AK&SK初始化账号Client
	 * @param accessKeyId
	 * @param accessKeySecret
	 * @return Client
	 * @throws Exception
	 */
	alidns: Alidns20150109
	constructor(accessKeyId: string, accessKeySecret: string) {
		const config = new $OpenApi.Config({ accessKeyId, accessKeySecret })
		config.endpoint = `alidns.cn-hangzhou.aliyuncs.com`
		this.alidns = new Alidns20150109(config)
	}

	async getDomainRecorders(domainName: string) {
		return await this.alidns.describeDomainRecordsWithOptions(
			new $Alidns20150109.DescribeDomainRecordsRequest({ domainName }),
			new $Util.RuntimeOptions({})
		)
	}

	async getDomainIpv6Record(domainName: string) {
		const records = await this.getDomainRecorders(domainName)
		const ipv6Record = records.body.domainRecords?.record?.find(item => item.type === 'AAAA')
		if (!ipv6Record) return Promise.reject(`${domainName} 记录获取失败`)
		return ipv6Record
	}

	async updateDomainRecored(newReocrder: any) {
		return await this.alidns.updateDomainRecordWithOptions(
			new $Alidns20150109.UpdateDomainRecordRequest({
				lang: 'zh-CN',
				...newReocrder
			}),
			new $Util.RuntimeOptions({})
		)
	}

	async updateIpv6Record(domainName: string) {
		const record = await this.getDomainIpv6Record(domainName)
		if (!record) {
			logger(`获取 ${domainName} 记录失败`)
			return Promise.reject(`获取 ${domainName} 记录失败`)
		}

		const network = _.flatMap(networkInterfaces()).find(item => item?.family === 'IPv6' && /^(24)/.test(item.address))
		if (!network) return Promise.reject('获取本机 IPV6 地址失败')
		if (network.address === record.value) return logger('IPV6 地址无变更')

		record.value = network.address
		await this.updateDomainRecored({
			lang: 'zh-CN',
			recordId: record.recordId,
			RR: 'ipv6',
			type: 'AAAA',
			value: record.value
		})

		logger(`更新 IPV6 解析地址：${record.value}`)
	}
}

try {
	const client = new Client(accessKeyId, accessKeySecret)
	client.updateIpv6Record(DOMAIN_NAME)
	setInterval(() => client.updateIpv6Record(DOMAIN_NAME), 600000)
} catch (err) {
	logger(JSON.stringify(err))
}
