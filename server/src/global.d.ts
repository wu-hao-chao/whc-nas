import { DefaultContext, ExtendableContext } from 'koa'
import db from './db'

declare module 'koa' {
	interface ExtendableContext {
		return: (data: any, code = 200) => void
		success: (response: { code?: number; data?: any; msg: string } | string) => void
		info: (response: { code?: number; data?: any; msg: string } | string) => void
		warning: (response: { code?: number; data?: any; msg: string } | string) => unknown
		error: (response: { code?: number; data?: any; msg: string } | string) => unknown

		userInfo?: {
			id: number
			phone: string
			name: string
			createTime: string
		}

		db: {
			/**
			 * @description 查询数据库
			 * @param query — 查询语句
			 * @example
			 * db.select(`SELECT * FROM ${table}`)
			 * db.select(`SELECT field1, field2 FROM ${table}`)
			 */
			select: (query: string) => Promise<any[]>
			/**
			 * @description 获取行数
			 * @param query - 查询语句
			 * @example db.select(`SELECT COUNT(*) FROM ${table_name}`)
			 */
			count: (query: string) => Promise<number>
			/**
			 * @description 查询数据库一句
			 * @param query — 查询语句
			 * @example db.selectOne(`SELECT * FROM ${table}`) >>> db.select(`SELECT * FROM ${table} LIMIT 1`)
			 */
			selectOne: (query: string) => Promise<any>
			/**
			 * @description 插入数据
			 * @param query - 插入语句
			 * @example db(`INSERT INTO ${table} (field1, field2) VALUES (${value1}, ${value2})`)
			 * @returns 插入的 id 值
			 */
			insert: (query: string) => Promise<number>
			/**
			 * @description 更新数据
			 * @param query — 查询语句
			 * @example db(`UPDATE ${table} SET field1=${value1}, field2=${value2} WHERE id=${id}`)
			 * @returns 更新了多少条数据
			 */
			update: (query: string) => Promise<number>
			/**
			 * @description 删除数据
			 * @param query — 查询语句
			 * @example db(`DELETE FROM ${table} WHERE id=${id}`)
			 * @returns 删除了多少条数据
			 */
			delete: (query: string) => Promise<number>
		}
	}
}
