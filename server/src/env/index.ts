import dev from './dev.env'
import prod from './prod.env'

const envType = process.env.NODE_ENV as 'dev' | 'prod'
const envObj = envType === 'dev' ? dev : prod

export default Object.freeze({
	envType,
	...envObj
})
