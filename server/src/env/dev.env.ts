export default Object.freeze({
	PROTOCOL: 'http',
	DOMAIN_NAME: '127.0.0.1',
	PROT: 5407,
	IPV6_PROT: 443,
	DB_HOST: 'localhost',
	DB_USER: 'root',
	DB_PASSWORD: 'root',
	DB_DATABASE: 'pi'
})
